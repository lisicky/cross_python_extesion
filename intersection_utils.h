#ifndef INTERSECTION_UTILS
#define INTERSECTION_UTILS

typedef struct Point3D
{
	float x;
	float y;
	float z;
} POINT3D;

typedef struct SkewLinesInfo
{
	POINT3D cross_point;
	float cross_dist;
	int is_rays_intersects;
	float middle_point_dist;
} SKEWLINEINFO;

SKEWLINEINFO get_lines_distances(const POINT3D *start_point_one, const POINT3D *end_point_one, const POINT3D *start_point_two, const POINT3D *end_point_two, int as_rays, int get_middle_point_dist);

#endif