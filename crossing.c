#include <stdio.h>
#include <Python.h>
#include "numpy/ndarraytypes.h"
#include "numpy/ufuncobject.h"
#include "numpy/npy_3kcompat.h"
#include "intersection_utils.h"

static POINT3D get_point_from_array(PyObject* arr);
static int get_numpy_array(PyObject* pList, PyObject** outArr1, PyObject** outArr2);
static void fill_array(POINT3D* point, PyObject* arr);
static int check_dimenstion_and_count(PyObject* arr);

static PyObject* crossing_c(PyObject *dummy, PyObject *args)
{
	int use_ray;
	int get_middle_dist;
	int has_error = 0;
	PyObject *pList1 = NULL;
	PyObject *pList2 = NULL;
	PyObject *arr1 = NULL;
	PyObject *arr2 = NULL;
	PyObject *arr3 = NULL;
	PyObject *arr4 = NULL;
	PyObject* out_array = NULL;
	POINT3D point1;
	POINT3D point2;
	POINT3D point3;
	POINT3D point4;

	if (!PyArg_ParseTuple(args, "O!O!pp", &PyList_Type, &pList1, &PyList_Type, &pList2, &use_ray, &get_middle_dist))
	{
		PyErr_SetString(PyExc_TypeError, "error while parsing parameters");
		return NULL;
	}

	if (!get_numpy_array(pList1, &arr1, &arr2))
		has_error = 1;
	if (!has_error && !get_numpy_array(pList2, &arr3, &arr4))
		has_error = 1;
	if (!has_error && !check_dimenstion_and_count(arr1))
		has_error = 1;
	if (!has_error && !check_dimenstion_and_count(arr2))
		has_error = 1;
	if (!has_error && !check_dimenstion_and_count(arr3))
		has_error = 1;
	if (!has_error && !check_dimenstion_and_count(arr4))
		has_error = 1;

	if (!has_error)
	{
		point1 = get_point_from_array(arr1);
		point2 = get_point_from_array(arr2);
		point3 = get_point_from_array(arr3);
		point4 = get_point_from_array(arr4);

		SKEWLINEINFO lines_info = get_lines_distances(&point1, &point2, &point3, &point4, use_ray, get_middle_dist);

		int out_list_size = 4;
		out_array = PyList_New(out_list_size);

		if (lines_info.cross_dist < 0)
		{
			//stub
			for (int i = 0; i < out_list_size; ++i)
			{
				Py_INCREF(Py_None);
				PyList_SetItem(out_array, i, Py_None);
			}
		}
		else
		{
			npy_intp dims[1] = { 3 };
			PyObject* cross_point = PyArray_SimpleNew(1, dims, NPY_FLOAT64);
			fill_array(&lines_info.cross_point, cross_point);

			PyList_SetItem(out_array, 0, cross_point);
			PyList_SetItem(out_array, 1, PyFloat_FromDouble(lines_info.cross_dist));
			if (!use_ray)
			{
				Py_INCREF(Py_None);
				PyList_SetItem(out_array, 2, Py_None);
			}
			else
			{
				PyObject* is_rays_intersects = lines_info.is_rays_intersects ? Py_True : Py_False;
				Py_INCREF(is_rays_intersects);
				PyList_SetItem(out_array, 2, is_rays_intersects);
			}
			if (!get_middle_dist)
			{
				Py_INCREF(Py_None);
				PyList_SetItem(out_array, 3, Py_None);
			}
			else
			{
				PyList_SetItem(out_array, 3, PyFloat_FromDouble(lines_info.middle_point_dist));
			}
		}
	}

	Py_XDECREF(arr1);
	Py_XDECREF(arr2);
	Py_XDECREF(arr3);
	Py_XDECREF(arr4);

	return out_array;
}

static POINT3D get_point_from_array(PyObject* arr)
{
	POINT3D point;
	npy_float64* data = (npy_float64 *)PyArray_DATA(arr);
	point.x = (float) *(data++);
	point.y = (float)  *(data++);
	point.z = (float)*data;
	return point;
}

static void fill_array(POINT3D* point, PyObject* arr)
{
	npy_float64* data = (npy_float64 *)PyArray_DATA(arr);
	*(data++) = point->x;
	*(data++) = point->y;
	*data = point->z;
}

static int get_numpy_array(PyObject* pList, PyObject** outArr1, PyObject** outArr2)
{
	int n = PyList_Size(pList);
	if (n != 2)
	{
		PyErr_SetString(PyExc_Exception, "invalid input data");
		return 0;
	};

	(*outArr1) = PyArray_FROM_OTF(PyList_GetItem(pList, 0), NPY_FLOAT64, NPY_IN_ARRAY);
	(*outArr2) = PyArray_FROM_OTF(PyList_GetItem(pList, 1), NPY_FLOAT64, NPY_IN_ARRAY);
	if (*outArr1 == NULL || *outArr2 == NULL)
	{
		PyErr_SetString(PyExc_TypeError, "invalid input data");
		return 0;
	}
	return 1;
}

static int check_dimenstion_and_count(PyObject* arr)
{
	int dimension = PyArray_NDIM(arr);
	if (dimension != 1)
	{
		PyErr_SetString(PyExc_TypeError, "invalid dimension of input array. Must be = 1");
		return 0;
	}

	int element_count = (int)PyArray_SHAPE((PyArrayObject*)arr)[0];
	if (element_count != 3)
	{
		PyErr_SetString(PyExc_TypeError, "invalid element count in input array. Must be = 3");
		return 0;
	}
	return 1;
}

static PyMethodDef crossing_methods[] = {
	{
		"crossing", crossing_c, METH_VARARGS,
		"get cross point",
	},
	{ NULL, NULL, 0, NULL }
};


static struct PyModuleDef crossing_module = {
	PyModuleDef_HEAD_INIT,
	"crossing",
	"A Python module for test task",
	-1,
	crossing_methods
};


PyMODINIT_FUNC PyInit_crossing(void) {
	Py_Initialize();
	import_array();
	return PyModule_Create(&crossing_module);
}