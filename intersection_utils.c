//ORIGINAL CODE http://geomalgorithms.com/a07-_distance.html
#include <math.h>
#include "intersection_utils.h"

#define SMALL_NUM   0.00000001 // anything that avoids division overflow

inline float dot(const POINT3D* u, const POINT3D* v)
{
	return (u->x * v->x + u->y * v->y + u->z * v->z);
}

inline POINT3D sub_points(const POINT3D* u, const POINT3D* v)
{
	POINT3D result;
	result.x = u->x - v->x;
	result.y = u->y - v->y;
	result.z = u->z - v->z;
	return result;
}

inline float norm(const POINT3D* v)
{
	return sqrtf(dot(v, v));
}

inline POINT3D mul_point(const POINT3D* point, float scalar)
{
	POINT3D res_point;
	res_point.x = point->x * scalar;
	res_point.y = point->y * scalar;
	res_point.z = point->z * scalar;
	return res_point;
}

inline POINT3D sum_points(const POINT3D* u, const POINT3D* v)
{
	POINT3D result;
	result.x = v->x + u->x;
	result.y = v->y + u->y;
	result.z = v->z + u->z;
	return result;
}

inline float dist(const POINT3D* u, const POINT3D* v)
{
	POINT3D sub_vec = sub_points(u, v);
	return norm(&sub_vec);
}

inline int is_on_segment(const POINT3D* a, const POINT3D* b, const POINT3D* p)
{
	float ab = sqrtf((b->x - a->x)*(b->x - a->x) + (b->y - a->y)*(b->y - a->y) + (b->z - a->z)*(b->z - a->z));
	float ap = sqrtf((p->x - a->x)*(p->x - a->x) + (p->y - a->y)*(p->y - a->y) + (p->z - a->z)*(p->z - a->z));
	float pb = sqrtf((b->x - p->x)*(b->x - p->x) + (b->y - p->y)*(b->y - p->y) + (b->z - p->z)*(b->z - p->z));

	if (fabsf(ab - (ap + pb) < SMALL_NUM))
		return 1;
	else
		return 0;
}

inline int is_on_segment_swap(const POINT3D* a, const POINT3D* b, const POINT3D* p)
{
	if (is_on_segment(a, b, p) || is_on_segment(a, p, b))
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

inline float dist_point_to_segment(const POINT3D* a, const POINT3D* b, const POINT3D* p)
{
	POINT3D v = sub_points(b, a);
	POINT3D w = sub_points(p, a);

	float c1 = dot(&w, &v);
	if (c1 <= 0)
		return dist(p, a);

	float c2 = dot(&v, &v);
	if (c2 <= c1)
		return dist(p, b);

	double koeff = c1 / c2;
	POINT3D mul_vect = mul_point(&v, koeff);
	POINT3D Pb = sum_points(a, &mul_vect);
	return dist(p, &Pb);
}


SKEWLINEINFO get_lines_distances(const POINT3D *start_point_one, const POINT3D *end_point_one, const POINT3D *start_point_two, const POINT3D *end_point_two, int as_rays, int get_middle_point_dist)
{
	SKEWLINEINFO ret_info;
	POINT3D  u = sub_points(end_point_one, start_point_one);
	POINT3D  v = sub_points(end_point_two, start_point_two);
	POINT3D  w = sub_points(start_point_one, start_point_two);
	float    a = dot(&u, &u);
	float    b = dot(&u, &v);
	float    c = dot(&v, &v);
	float    d = dot(&u, &w);
	float    e = dot(&v, &w);
	float    determ = a*c - b*b;


	float    sc;
	float	 tc;

	// compute the line parameters of the two closest points
	if (determ < SMALL_NUM) {          // the lines are almost parallel
		ret_info.cross_dist = -1;
		ret_info.is_rays_intersects = -1;
		ret_info.middle_point_dist = -1;
		return ret_info;
	}
	else {
		sc = (b*e - c*d) / determ;
		tc = (a*e - b*d) / determ;
	}

	POINT3D projU = mul_point(&u, sc);
	POINT3D projV = mul_point(&v, tc);

	POINT3D nearest_first = sum_points(start_point_one, &projU);
	POINT3D nearest_second = sum_points(start_point_two, &projV);
	POINT3D nearest_sum = sum_points(&nearest_first, &nearest_second);
	ret_info.cross_point = mul_point(&nearest_sum, 0.5);
	ret_info.cross_dist = dist(&nearest_first, &nearest_second);

	if (as_rays)
	{
		if (is_on_segment_swap(start_point_one, end_point_one, &nearest_first) &&
			is_on_segment_swap(start_point_two, end_point_two, &nearest_second))
		{
			ret_info.is_rays_intersects = 1;
		}
		else
		{
			ret_info.is_rays_intersects = 0;
		}
	}
	else
	{
		ret_info.is_rays_intersects = -1;
	}

	if (get_middle_point_dist)
	{
		ret_info.middle_point_dist = dist_point_to_segment(start_point_one, start_point_two, &ret_info.cross_point);
	}
	else
	{
		ret_info.middle_point_dist = -1;
	}

	return ret_info;
}

